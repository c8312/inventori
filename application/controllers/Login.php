<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_login', 'm_login');
    }


	public function index()
	{
		$this->_has_login();
		$this->load->view('v-login');
	}

	private function _has_login()
    {
        if ($this->session->has_userdata('login_session')) {
            redirect('dashboard');
        }
    }


	public function login_aksi()
	{
		$input = $this->input->post(null, true);

		$cek_username = $this->m_login->cek_username($input['username']);
		if (!empty($cek_username)) {
			$password = $this->m_login->get_password($input['username']);
		// 	var_dump($cek_username);
		// die;
			if (password_verify($input['password'], $password)) {
				$user_db = $this->m_login->userdata($input['username']);
				$userdata = [
					'user'  => $user_db['id_user'],
					'username'  => $user_db['username'],
					'role'  => $user_db['role'],
					'timestamp' => time()
				];
				$this->session->set_userdata('login_session', $userdata);
				redirect('admin/dashboard');
			} else {
				$this->session->set_flashdata('flash_error', 'Username / password salah');
				redirect('login');
			}
		} else {
			$this->session->set_flashdata('flash_error', 'Username / password salah');
			redirect('login');
		}
	}

	public function logout()
    {
        $this->session->unset_userdata('login_session');

        $this->session->set_flashdata('flash_error', 'Berhasil logout');
        redirect('login');
    }

}