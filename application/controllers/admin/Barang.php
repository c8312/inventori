<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        cek_login('admin');
        // model
        $this->load->model('admin/M_barang', 'barang');
    }
    
	public function index()
	{
		$data_user = $this->session->userdata('login_session');
		/* start of pagination --------------------- */
        $this->load->library('pagination');
        // pagination
        $config['base_url'] = site_url('/admin/barang/');
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->barang->get_total_data();
		$config['per_page'] = 5;

		$config['attributes'] = array('class' => 'page-link');
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config);
		$limit = $config['per_page'];
		$offset = (int) html_escape($this->input->get('per_page'));

        // data barang
        $params = array($offset, $limit);
		$data['rs_barang'] = $this->barang->get_all_data_barang($params);
		

		// title
		$data['no'] = $offset+1;

		$data['title'] = 'Barang';
		$this->load->view('template/admin/header', $data);
		$this->load->view('admin/barang/index');
		$this->load->view('template/admin/footer');
	}

	public function get_barang_by_id($id_barang)
	{
		$barang = $this->barang->get_detail_barang($id_barang);
		// var_dump($barang);
		// die;
		echo json_encode($barang);
	}

	public function insert()
	{
		
		$data = $this->input->post(null, true);

		$data_barang = [
			'nama' => $data['nama'],
			// 'jumlah_barang' => $data['jumlah'],
			'harga_barang' => $data['harga']
		];

		if ($this->barang->insert($data_barang)) {
			$this->session->set_flashdata('flash', 'Berhasil tambah');
			redirect('admin/barang');
		} else {
			$this->session->set_flashdata('flash', 'Gagal');
			redirect('admin/barang');
		}


	}

	public function update()
	{
		$data = $this->input->post(null, true);
		
		$data_barang = [
			'id_barang' =>  $data['id_barang'],
			'nama' => $data['nama'],
			// 'jumlah_barang' => $data['jumlah'],
			'harga_barang' => $data['harga']
		];
		if($this->barang->update($data_barang)){
			$this->session->set_flashdata('flash', 'Berhasil update');
			redirect('admin/barang');
			
		} else {
			$this->session->set_flashdata('flash', 'gagal');
			redirect('admin/barang');
		}
	}

	public function delete()
	{
		$data = $this->input->post(null, true);
		// 
		$data_barang = [
			'id_barang' => $data['id_barang'],
		];
		if($this->barang->delete($data_barang)){
			$this->session->set_flashdata('flash', 'Berhasil hapus');
			redirect('admin/barang');
		} else {
			$this->session->set_flashdata('flash', 'gagal');
			redirect('admin/barang');
		}
	}

}