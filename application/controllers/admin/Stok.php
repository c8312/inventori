<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        cek_login('admin');
        // model
        $this->load->model('admin/M_stok', 'stok');
    }
    
	public function index()
	{
		$data_user = $this->session->userdata('login_session');
		$search = $this->session->userdata('search_stok');

		$id_barang = empty($search['id_barang']) ? '%' : $search['id_barang'];
		$bulan = empty($search['bulan']) ? str_replace('0', '', date('m'))  : $search['bulan'];
		$tahun = empty($search['tahun']) ? date('Y') : $search['tahun'];

		$search['id_barang'] = $id_barang;
		$search['bulan'] = $bulan;
		$search['tahun'] = $tahun;
		
		/* start of pagination --------------------- */
        $this->load->library('pagination');
        // pagination
        $config['base_url'] = site_url('/admin/stok/');
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->stok->get_total_data(array($id_barang, $bulan, $tahun));
		$config['per_page'] = 5;

		$config['attributes'] = array('class' => 'page-link');
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config);
		$limit = $config['per_page'];
		$offset = (int) html_escape($this->input->get('per_page'));

        // data stok
        $params = array($id_barang, $bulan, $tahun, $offset, $limit);
		$data['rs_stok'] = $this->stok->get_all_data_stok($params);
        $data['nama_barang'] = $this->stok->get_all_data_barang();

		// title
		$data['no'] = $offset+1;

		$data['title'] = 'Stok';
		$data['search'] = $search;
		$data['rs_tahun'] = $this->stok->get_tahun();
		$this->load->view('template/admin/header', $data);
		$this->load->view('admin/stok/index');
		$this->load->view('template/admin/footer');
	}

	public function cari()
	{
		$data = $this->input->post(null, true);
        // session
        if ($this->input->post('save') == "cari") {
            // params
            $params = array(
                "id_barang" => $data['id_barang'],
                "bulan" => $data['bulan'],
                "tahun" => $data['tahun'],
            );
            // set session
            $this->session->set_userdata("search_stok", $params);
        } else {
            // unset session
            $this->session->unset_userdata("search_stok");
        }
        // redirect
        redirect("admin/stok");
	}

	public function get_stok_by_id($id_stok)
	{
		$stok = $this->stok->get_detail_stok($id_stok);
		$stok['jum_saat_ini'] = $this->stok->get_jum_barang_saat_ini($stok['id_barang']);
		echo json_encode($stok);
	}

	public function insert()
	{
		
		$data = $this->input->post(null, true);

		$data_stok = [
			'id_barang' => $data['id_barang'],
			'jumlah_stok' => $data['jumlah'],
			'tgl_stok' => $data['tgl']
		];

		if ($this->stok->insert($data_stok)) {
			// data barang
			$data_barang = [
				'id_barang' => $data['id_barang'],
				'jumlah_barang' => $data['jumlah_total']
			];
			$this->stok->update_barang($data_barang);
			$this->session->set_flashdata('flash', 'Berhasil tambah');
			redirect('admin/stok');
		} else {
			$this->session->set_flashdata('flash', 'Gagal');
			redirect('admin/stok');
		}

	}

	public function update()
	{
		$data = $this->input->post(null, true);
		
		$data_stok = [
			'id_stok' =>  $data['id_stok'],
			'jumlah_stok' => $data['res_jumlah_edit'],
			'tgl_stok' => $data['tgl']
		];
		
		if($this->stok->update($data_stok)){
			// perubahan data barang
			$data_barang = [
				'id_barang' => $data['id_barang'],
				'jumlah_barang' => $data['res_jumlah_barang']
			];
			$this->stok->update_barang($data_barang);
			// 
			$this->session->set_flashdata('flash', 'Berhasil update');
			redirect('admin/stok');
			
		} else {
			$this->session->set_flashdata('flash', 'gagal');
			redirect('admin/stok');
		}
	}

	public function delete()
	{
		$data = $this->input->post(null, true);
		// 
		$data_stok = [
			'id_stok' => $data['id_stok'],
		];
		if($this->stok->delete($data_stok)){
			// perubahan data barang
			$ress = $this->stok->update_barang_2($data['jumlah'], $data['id_barang']);
			if ($ress) {
				$this->session->set_flashdata('flash', 'Berhasil hapus');
				redirect('admin/stok');
			}
		} else {
			$this->session->set_flashdata('flash', 'gagal');
			redirect('admin/stok');
		}
	}

}