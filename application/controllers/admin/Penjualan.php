<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        cek_login('admin');
        // model
        $this->load->model('admin/M_penjualan', 'penjualan');
    }
    
	public function index()
	{
		$data_user = $this->session->userdata('login_session');
		$search = $this->session->userdata('search_penjualan');

		$id_barang = empty($search['id_barang']) ? '%' : $search['id_barang'];
		$bulan = empty($search['bulan']) ? str_replace('0', '', date('m'))  : $search['bulan'];
		$tahun = empty($search['tahun']) ? date('Y') : $search['tahun'];

		$search['id_barang'] = $id_barang;
		$search['bulan'] = $bulan;
		$search['tahun'] = $tahun;
		/* start of pagination --------------------- */
        $this->load->library('pagination');
        // pagination
        $config['base_url'] = site_url('/admin/penjualan/');
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->penjualan->get_total_data(array($id_barang, $bulan, $tahun));
		$config['per_page'] = 5;

		$config['attributes'] = array('class' => 'page-link');
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config);
		$limit = $config['per_page'];
		$offset = (int) html_escape($this->input->get('per_page'));

        // data penjualan
        $params = array($id_barang, $bulan, $tahun, $offset, $limit);
		$data['rs_penjualan'] = $this->penjualan->get_all_data_penjualan($params);
        $data['nama_barang'] = $this->penjualan->get_all_data_barang();

		// title
		$data['no'] = $offset+1;

		$data['title'] = 'Penjualan';
		$data['search'] = $search;
		$data['rs_tahun'] = $this->penjualan->get_tahun();
		$this->load->view('template/admin/header', $data);
		$this->load->view('admin/penjualan/index');
		$this->load->view('template/admin/footer');
	}

	public function cari()
	{
		$data = $this->input->post(null, true);
        // session
        if ($this->input->post('save') == "cari") {
            // params
            $params = array(
                "id_barang" => $data['id_barang'],
                "bulan" => $data['bulan'],
                "tahun" => $data['tahun'],
            );
            // set session
            $this->session->set_userdata("search_penjualan", $params);
        } else {
            // unset session
            $this->session->unset_userdata("search_penjualan");
        }
        // redirect
        redirect("admin/penjualan");
	}

	public function get_penjualan_by_id($id_penjualan)
	{
		$penjualan = $this->penjualan->get_detail_penjualan($id_penjualan);
		echo json_encode($penjualan);
	}

	public function insert()
	{
		
		$data = $this->input->post(null, true);

		$data_penjualan = [
			'id_barang' => $data['id_barang'],
			'harga_jual' => $data['harga_jual'],
			'jumlah_jual' => $data['jumlah_jual'],
			'tgl_jual' => $data['tgl_jual']
		];

		if ($this->penjualan->insert($data_penjualan)) {
			// data barang
			$data_barang = [
				'id_barang' => $data['id_barang'],
				'jumlah_barang' => $data['sisa_barang']
			];
			$this->penjualan->update_barang($data_barang);
			$this->session->set_flashdata('flash', 'Berhasil tambah');
			redirect('admin/penjualan');
		} else {
			$this->session->set_flashdata('flash', 'Gagal');
			redirect('admin/penjualan');
		}

	}

	public function update()
	{
		$data = $this->input->post(null, true);
		
		$data_penjualan = [
			'id_penjualan' =>  $data['id_penjualan'],
			'id_barang' =>  $data['barang'],
			'jumlah_penjualan' => $data['jumlah'],
			'tgl_penjualan' => $data['tgl']
		];
		if($this->penjualan->update($data_penjualan)){
			$this->session->set_flashdata('flash', 'Berhasil update');
			redirect('admin/penjualan');
			
		} else {
			$this->session->set_flashdata('flash', 'gagal');
			redirect('admin/penjualan');
		}
	}

	public function delete()
	{
		$data = $this->input->post(null, true);
		// 
		$data_penjualan = [
			'id_penjualan' => $data['id_penjualan'],
		];
		if($this->penjualan->delete($data_penjualan)){
			$this->session->set_flashdata('flash', 'Berhasil hapus');
			redirect('admin/penjualan');
		} else {
			$this->session->set_flashdata('flash', 'gagal');
			redirect('admin/penjualan');
		}
	}

}