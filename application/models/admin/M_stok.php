<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_stok extends CI_Model
{
    public function get_all_data_stok($params)
    {
        $sql = "SELECT *
        FROM stok a
        INNER JOIN barang b ON a.id_barang = b.id_barang
        WHERE a.id_barang LIKE ?
        AND MONTH(a.tgl_stok) LIKE ?
        AND YEAR(a.tgl_stok) LIKE ? 
        ORDER BY b.nama ASC
        LIMIT ?, ?";
        $query = $this->db->query($sql, $params)->result_array();
        return $query;
    }

    public function get_all_data_barang()
    {
        $sql = "SELECT *
        FROM barang";
        $query = $this->db->query($sql)->result_array();
        return $query;
    }

    public function get_detail_stok($params)
    {
       $sql = "SELECT a.*, b.nama FROM stok a
                INNER JOIN barang b ON a.id_barang = b.id_barang
               WHERE a.id_stok = ?";
       $query = $this->db->query($sql, $params);
       $result = $query->row_array();
       return $result;

    }

    public function get_tahun()
    {
        $sql = "SELECT YEAR(a.tgl_stok)'tahun' FROM stok a
                GROUP BY YEAR(a.tgl_stok)";
        $query = $this->db->query($sql)->result_array();
        return $query;
    }

    public function get_jum_barang_saat_ini($params)
    {
        $sql = "SELECT SUM(a.jumlah_barang)'jum_barang' FROM barang a WHERE a.id_barang = ? ";
        $query = $this->db->query($sql, $params);
        $result = $query->row_array();
        return $result['jum_barang'];
    }
    
    public function get_total_data($params)
    {
        $sql = "SELECT COUNT(*)'total'
                FROM stok a
                INNER JOIN barang b ON a.id_barang = b.id_barang
                WHERE a.id_barang LIKE ?
                AND MONTH(a.tgl_stok) LIKE ?
                AND YEAR(a.tgl_stok) LIKE ? ";
        $query = $this->db->query($sql, $params)->row_array();
        return $query['total'];
    }


    public function insert($params)
    {
        $this->db->insert('stok', $params);
        return $this->db->insert_id();
    }

    public function insert_user($params)
    {
        return $this->db->insert('user', $params);
    }

    public function update($params)
    {
        $this->db->where('id_stok', $params['id_stok']);
        return $this->db->update('stok', $params);
    }

    public function update_barang($params)
    {
        $this->db->where('id_barang', $params['id_barang']);
        return $this->db->update('barang', $params);
    }

    public function update_barang_2($jumlah, $id_barang)
    {
        $sql = "UPDATE barang a SET a.jumlah_barang = a.jumlah_barang - ".$jumlah." WHERE a.id_barang = ".$id_barang." ";
        $query = $this->db->query($sql);
        return $query;
    }

    public function delete($params)
    {
        $this->db->where('id_stok', $params['id_stok']);
        return $this->db->delete('stok', $params);
    }

    public function delete_user($params)
    {
        $this->db->where('id_user', $params['id_user']);
        return $this->db->delete('user', $params);
    }
    
    public function get($table, $data = null, $where = null)
    {
        if ($data != null) {
            return $this->db->get_where($table, $data)->row_array();
        } else {
            return $this->db->get_where($table, $where)->result_array();
        }
    }
}