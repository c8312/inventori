<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_dashboard extends CI_Model
{
    public function get_summary()
    {
        $sql = "SELECT * FROM (
                SELECT COUNT(*)'jum_barang' FROM barang) AS barang,
                (SELECT COUNT(*)'jum_penjualan' FROM penjualan) AS penjualan,
                (SELECT COUNT(*)'jum_stok' FROM stok) AS stok";
        $query = $this->db->query($sql)->row_array();
        return $query;
    }
    
}