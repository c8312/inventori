<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_barang extends CI_Model
{
    public function get_all_data_barang($params)
    {
        $sql = "SELECT * FROM barang a
                LIMIT ?,?";
        $query = $this->db->query($sql, $params)->result_array();
        return $query;
    }

    public function get_detail_barang($params)
    {
       $sql = "SELECT * FROM barang WHERE id_barang = ?";
       $query = $this->db->query($sql, $params);
       $result = $query->row_array();
       return $result;

    }
    
    public function get_total_data()
    {
        $sql = "SELECT COUNT(*)'total' 
                FROM barang a";
        $query = $this->db->query($sql)->row_array();
        return $query['total'];
    }

    public function insert($params)
    {
        $this->db->insert('barang', $params);
        return $this->db->insert_id();
    }

    public function insert_user($params)
    {
        return $this->db->insert('user', $params);
    }

    public function update($params)
    {
        $this->db->where('id_barang', $params['id_barang']);
        return $this->db->update('barang', $params);
    }

    public function update_user($params)
    {
        $this->db->where('id_user', $params['id_user']);
        return $this->db->update('user', $params);
    }

    public function delete($params)
    {
        $this->db->where('id_barang', $params['id_barang']);
        return $this->db->delete('barang', $params);
    }

    public function delete_user($params)
    {
        $this->db->where('id_user', $params['id_user']);
        return $this->db->delete('user', $params);
    }
    
}