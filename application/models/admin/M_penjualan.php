<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_penjualan extends CI_Model
{
    public function get_all_data_penjualan($params)
    {
        $sql = "SELECT a.id_penjualan, b.nama, b.harga_barang, a.jumlah_jual, a.harga_jual, a.tgl_jual
                FROM penjualan a
                INNER JOIN barang b ON a.id_barang = b.id_barang
                WHERE a.id_barang LIKE ?
                AND MONTH(a.tgl_jual) LIKE ?
                AND YEAR(a.tgl_jual) LIKE ? 
                LIMIT ?,?";
        $query = $this->db->query($sql, $params)->result_array();
        return $query;
    }

    public function get_detail_penjualan($params)
    {
       $sql = "SELECT * FROM penjualan WHERE id_penjualan = ?";
       $query = $this->db->query($sql, $params);
       $result = $query->row_array();
       return $result;

    }

    public function get_all_data_barang()
    {
        $sql = "SELECT *
        FROM barang";
        $query = $this->db->query($sql)->result_array();
        return $query;
    }

    public function get_tahun()
    {
        $sql = "SELECT YEAR(a.tgl_jual)'tahun' FROM penjualan a
                GROUP BY YEAR(a.tgl_jual)";
        $query = $this->db->query($sql)->result_array();
        return $query;
    }
    
    public function get_total_data($params)
    {
        $sql = "SELECT COUNT(*)'total' 
                FROM penjualan a
                WHERE a.id_barang LIKE ?
                AND MONTH(a.tgl_jual) LIKE ?
                AND YEAR(a.tgl_jual) LIKE ? ";
        $query = $this->db->query($sql, $params)->row_array();
        return $query['total'];
    }

    public function insert($params)
    {
        $this->db->insert('penjualan', $params);
        return $this->db->insert_id();
    }

    public function update($params)
    {
        $this->db->where('id_penjualan', $params['id_penjualan']);
        return $this->db->update('penjualan', $params);
    }

    public function update_barang($params)
    {
        $this->db->where('id_barang', $params['id_barang']);
        return $this->db->update('barang', $params);
    }

    public function delete($params)
    {
        $this->db->where('id_penjualan', $params['id_penjualan']);
        return $this->db->delete('penjualan', $params);
    }
    
}