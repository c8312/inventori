<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_login extends CI_Model
{

    public function cek_username($username)
    {
        $sql = "SELECT * FROM users a 
                WHERE a.username = ? ";
        $query = $this->db->query($sql, $username)->row_array();
        return $query;
    }

    public function get_password($username)
    {
        // cari dari mahasiswa
        $sql = "SELECT a.password FROM users a 
                WHERE a.username = ? ";
        $query = $this->db->query($sql, $username)->row_array();
        return $query['password'];
    }

    public function userdata($username)
    {
        return $this->db->get_where('users', ['username' => $username])->row_array();
    }
}