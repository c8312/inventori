<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>admin/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Default box -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <form action="<?= base_url() ?>admin/stok/cari" method="POST">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#modal-add" style="width: 60%">
                                            <i class="fa fa-plus"></i> Tambah Data
                                        </button>
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <select class="form-control" name="id_barang">
                                            <option value="">Pilih Barang</option>
                                            <?php foreach ($nama_barang as $key => $nm) { ?>
                                            <option value="<?= $nm['id_barang'] ?>" <?php if($search['id_barang'] == $nm['id_barang']) { echo "selected"; } ?> ><?= $nm['nama'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <select class="form-control" name="bulan">
                                            <option value="">Pilih Bulan</option>
                                            <option value="1" <?php if($search['bulan'] == '1') { echo "selected"; } ?> >January</option>
                                            <option value="2" <?php if($search['bulan'] == '2') { echo "selected"; } ?> >February</option>
                                            <option value="3" <?php if($search['bulan'] == '3') { echo "selected"; } ?> >Maret</option>
                                            <option value="4" <?php if($search['bulan'] == '4') { echo "selected"; } ?> >April</option>
                                            <option value="5" <?php if($search['bulan'] == '5') { echo "selected"; } ?> >Mei</option>
                                            <option value="6" <?php if($search['bulan'] == '6') { echo "selected"; } ?> >Juni</option>
                                            <option value="7" <?php if($search['bulan'] == '7') { echo "selected"; } ?> >Juli</option>
                                            <option value="8" <?php if($search['bulan'] == '8') { echo "selected"; } ?> >Agustus</option>
                                            <option value="9" <?php if($search['bulan'] == '9') { echo "selected"; } ?> >September</option>
                                            <option value="10" <?php if($search['bulan'] == '10') { echo "selected"; } ?> >Oktober</option>
                                            <option value="11" <?php if($search['bulan'] == '11') { echo "selected"; } ?> >Novenber</option>
                                            <option value="12" <?php if($search['bulan'] == '12') { echo "selected"; } ?> >Desember</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <select class="form-control" name="tahun">
                                            <option value="">Pilih Tahun</option>
                                            <?php foreach ($rs_tahun as $key => $th) { ?>
                                            <option value="<?= $th['tahun'] ?>" <?php if($search['tahun'] == $th['tahun'] ) { echo "selected"; } ?> ><?= $th['tahun'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="submit" name="save" value="cari" class="btn btn-block btn-success btn-sm" style="width: 100%">
                                            <i class="fa fa-search"></i> Cari
                                        </button>
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="submit" name="save" value="reset" class="btn btn-block btn-dark btn-sm" style="width: 100%">
                                            <i class="fa fa-times"></i> Reset
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width: 10px">No</th>
                                        <th>Nama Barang</th>
                                        <th>Jumlah Stok</th>
                                        <th>Tanggal Stok</th>
                                        <th style="width: 200px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($rs_stok as $key => $value) { ?>
                                    <tr>
                                        <td align="center"><?= $no++ ?></td>
                                        <td><?= $value['nama'] ?></td>
                                        <td><?= $value['jumlah_stok'] ?></td>
                                        <td><?= date('d F Y', strtotime($value['tgl_stok']))  ?> </td>
                                        <td align="center">
                                            <a class="btn btn-info btn-sm edit_mahasiswa" href="#"
                                                onclick="update(<?= $value['id_stok'] ?>)"><i
                                                    class="fas fa-pencil-alt"></i> Edit</a>
                                            <a class="btn btn-danger btn-sm" href="#"
                                                onclick="hapus(<?= $value['id_stok'] ?>)"><i class="fas fa-trash"></i>
                                                Delete</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <ul class="pagination pagination-sm m-0 float-right">
                                <?=  $this->pagination->create_links(); ?>
                                <!-- <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">&raquo;</a></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modal-add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data <?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url() ?>admin/stok/insert" method="POST">
                <div class="modal-body">

                    <div class="form-group">
                        <label for="id_barang_ad">Nama Barang</label>
                        <select class="form-control select2" id="id_barang_add" onchange="set_jumlah(this)" name="id_barang" required>
                            <option value="">---</option>
                            <?php foreach ($nama_barang as $key => $nm) { ?>
                            <option value="<?= $nm['id_barang'] ?>" data-jumlah="<?= $nm['jumlah_barang'] ?>"><?= $nm['nama'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Jumlah Barang</label>
                                <input type="number" readonly class="form-control" id="jumlah_barang_res" placeholder="Jumlah Barang" required>
                                <small>*ketersediaan barang saat ini</small>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="jumlah">Jumlah Stok</label>
                                <input type="number" class="form-control" name="jumlah" id="jumlah_add" onkeyup="hitung()" placeholder="Jumlah Stok">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="jumlah">Jumlah Total</label>
                                <input type="number" readonly class="form-control" name="jumlah_total" id="jumlah_total" placeholder="Jumlah Stok">
                                <small>*jumlah barang setelah stok</small>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tgl">Tanggal Stok</label>
                        <input type="date" class="form-control" name="tgl" id="tgl_add" placeholder="Tanggal Stok"
                            required>
                    </div>

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Data <?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url() ?>admin/stok/update" method="POST">
                <input type="hidden" name="id_stok" id="id_stok_edit" value="">
                <input type="hidden" name="id_barang" id="id_barang_edit" value="">
                <div class="modal-body">
                    
                    <div class="form-group">
                        <label for="nama_barang">Jumlah Stok Masuk</label>
                        <input type="text" class="form-control" placeholder="Nama barang" id="nama_barang" readonly>
                    </div>

                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label for="jumlah">Jumlah Stok Masuk</label>
                                <input type="number" class="form-control" name="jumlah" id="jumlah_edit" placeholder="Jumlah Stok" readonly>
                            </div>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label for="">Jumlah Barang Saat ini</label>
                                <input type="number" class="form-control" name="" id="jum_saat_ini" placeholder="Jumlah Barang Saat Ini" readonly="">
                                <small>*jumlah barang saat ini</small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label for="jumlah">Perubahan Stok Masuk</label>
                                <input type="number" class="form-control" name="res_jumlah_edit" id="res_jumlah_edit" onkeyup="hitung_edit()" placeholder="Jumlah Stok" required>
                                
                            </div>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label for="">Hasil Barang Saat ini</label>
                                <input type="number" class="form-control" name="res_jumlah_barang" id="res_jumlah_edit_2" placeholder="Jumlah Stok" required readonly="">
                                <small>*jumlah barang setelah mengalami perubahan</small>
                            </div>
                        </div>
                    </div>                    

                    <div class="form-group">
                        <label for="tgl">Tanggal Stok</label>
                        <input type="date" class="form-control" name="tgl" id="tgl_edit" placeholder="Tanggal Stok"
                            required>
                    </div>

                </div>

                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Data <?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url() ?>admin/stok/delete" method="POST">
                <input type="hidden" name="id_stok" id="id_stok_delete" value="">
                <input type="hidden" name="id_barang" id="id_barang_delete" value="">
                <div class="modal-body">

                    <div class="form-group">
                        <label for="id_barang_edit">Nama Barang</label>
                        <select class="form-control select2" id="id_barang_delete_2" name="barang" required disabled>
                            <option value="">---</option>
                            <?php foreach ($nama_barang as $nb) : ?>
                            <?php $selected = $stok['id_barang'] == $nb['id_barang'] ? 'selected' : ''; ?>
                            <option <?= $selected ?> value="<?= $nb['id_barang'] ?>">
                                <?= $nb['nama'] ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="jumlah">Jumlah Stok</label>
                        <input type="number" class="form-control" name="jumlah" id="jumlah_delete"
                            placeholder="Jumlah Stok" readonly>
                    </div>
                    <div class="form-group">
                        <label for="tgl">Tanggal Stok</label>
                        <input type="date" class="form-control" name="tgl" id="tgl_delete" placeholder="Tanggal Stok"
                            readonly>
                    </div>

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>

function update(id_stok) {
    var url = '<?= base_url() ?>admin/stok/get_stok_by_id/' + id_stok;
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function(result) {
            $('#nama_barang').val(result.nama);
            $('#id_stok_edit').val(result.id_stok);
            $('#id_barang_edit').val(result.id_barang);
            $('#jumlah_edit').val(result.jumlah_stok);
            $('#tgl_edit').val(result.tgl_stok);
            $('#jum_saat_ini').val(result.jum_saat_ini);
            $('#modal-edit').modal('show'); //now its working
        },
        error: function(error) {
            alert("fail");
        }
    });
}

function hapus(id_stok) {
    var url = '<?= base_url() ?>admin/stok/get_stok_by_id/' + id_stok;
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function(result) {
            $('#id_stok_delete').val(result.id_stok);
            $('#id_barang_delete').val(result.id_barang);
            $('#id_barang_delete_2').val(result.id_barang).change();
            $('#jumlah_delete').val(result.jumlah_stok);
            $('#tgl_delete').val(result.tgl_stok);
            $('#modal-delete').modal('show'); //now its working
        },
        error: function(result) {
            alert("fail");
        }
    });
}
</script>

<script type="text/javascript">
    function set_jumlah(selectObject){
        var jumlah_barang = selectObject.options[selectObject.selectedIndex].getAttribute('data-jumlah');; 
        document.getElementById("jumlah_barang_res").value = jumlah_barang;
    }

    function hitung(){
        const awal = document.getElementById("jumlah_barang_res").value;
        const tambah = document.getElementById("jumlah_add").value;
        const hasil = Number(awal) + Number(tambah);
        document.getElementById("jumlah_total").value = hasil;
    }

    function hitung_edit() {
        const hasil = '0';
        const awal = document.getElementById("jumlah_edit").value;
        const revisi = document.getElementById("res_jumlah_edit").value;
        const jum_saat_ini = document.getElementById("jum_saat_ini").value;
        if (Number(revisi) > Number(awal)) {
            // alert('lb');
            // alert(awal);
            // alert(revisi);
            const tambah = Number(revisi) - Number(awal);
            // alert(tambah);
            const hasil = Number(jum_saat_ini) + Number(tambah);
            // alert(hasil);
            document.getElementById("res_jumlah_edit_2").value = hasil;
        } else if(Number(revisi) < Number(awal) && Number(revisi) != 0){
            // alert('kr');
            // alert(awal);
            // alert(revisi);
            const tambah = Number(awal) - Number(revisi);
            // alert(tambah);
            const hasil = Number(jum_saat_ini) - Number(tambah);
            // alert(hasil);
            document.getElementById("res_jumlah_edit_2").value = hasil;
        } else if(Number(revisi) == 0 || Number(revisi) == null ) {
            // alert('sam')
            document.getElementById("res_jumlah_edit_2").value = '0';
        }

        
    }
</script>