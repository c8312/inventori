<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>admin/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Default box -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <!-- <h3 class="card-title">Data <?= $title ?></h3> -->
                            <h3 class="card-title"><button type="button" class="btn btn-block btn-primary btn-sm"
                                    data-toggle="modal" data-target="#modal-add"><i class="fa fa-plus"></i> Tambah
                                    Data</button></h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width: 10px">No</th>
                                        <th>Nama Barang</th>
                                        <th>Jumlah Barang</th>
                                        <th>Harga Barang</th>
                                        <th style="width: 200px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($rs_barang as $key => $value) { ?>
                                    <tr>
                                        <td align="center"><?= $no++ ?></td>
                                        <td><?= $value['nama'] ?></td>
                                        <td><?= $value['jumlah_barang'] ?></td>
                                        <td>Rp <?= number_format($value['harga_barang'], 0, ',', '.') ?></td>
                                        <td align="center">
                                            <a class="btn btn-info btn-sm edit_mahasiswa" href="#"
                                                onclick="update(<?= $value['id_barang'] ?>)"><i
                                                    class="fas fa-pencil-alt"></i> Edit</a>
                                            <!-- <a class="btn btn-danger btn-sm" href="#"
                                                onclick="hapus(<?= $value['id_barang'] ?>)"><i class="fas fa-trash"></i>
                                                Delete
                                            </a> -->
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <ul class="pagination pagination-sm m-0 float-right">
                                <?=  $this->pagination->create_links(); ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modal-add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data <?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url() ?>admin/barang/insert" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nama">Nama Barang</label>
                        <input type="text" class="form-control" name="nama" id="nama_add" placeholder="Nama Barang"
                            required>
                    </div>

                    <!-- <div class="form-group">
                        <label for="jumlah">Jumlah Barang</label>
                        <input type="number" class="form-control" name="jumlah" id="jumlah_add"
                            placeholder="Jumlah Barang" required>
                    </div> -->

                    <div class="form-group">
                        <label for="harga">Harga Barang</label>
                        <input type="text" class="form-control" name="harga" id="harga_add" placeholder="Harga Barang"
                            required>
                    </div>

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Data <?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url() ?>admin/barang/update" method="POST">
                <input type="hidden" name="id_barang" id="id_barang_edit" value="">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nama">Nama Barang</label>
                        <input type="text" class="form-control" name="nama" id="nama_edit" placeholder="Nama Barang"
                            required>
                    </div>
                    <!-- <div class="form-group">
                        <label for="jumlah">Jumlah Barang</label>
                        <input type="number" class="form-control" name="jumlah" id="jumlah_edit"
                            placeholder="Jumlah Barang" required>
                    </div> -->
                    <div class="form-group">
                        <label for="harga">Harga Barang</label>
                        <input type="text" class="form-control" name="harga" id="harga_edit" placeholder="Harga Barang"
                            required>
                    </div>

                </div>

                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Data <?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url() ?>admin/barang/delete" method="POST">
                <input type="hidden" name="id_barang" id="id_barang_delete" value="">
                <div class="modal-body">

                    <div class="form-group">
                        <label for="nama">Nama Barang</label>
                        <input type="text" class="form-control" name="nama" id="nama_delete" placeholder="Nama Barang"
                            readonly>
                    </div>
                    <div class="form-group">
                        <label for="jumlah">Jumlah Barang</label>
                        <input type="number" class="form-control" name="jumlah" id="jumlah_delete"
                            placeholder="Jumlah Barang" readonly>
                    </div>
                    <div class="form-group">
                        <label for="harga">Harga Barang</label>
                        <input type="number" class="form-control" name="harga" id="harga_delete"
                            placeholder="Harga Barang" readonly>
                    </div>

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
function update(id_barang) {
    var url = '<?= base_url() ?>admin/barang/get_barang_by_id/' + id_barang;
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function(result) {
            $('#id_barang_edit').val(result.id_barang);
            $('#nama_edit').val(result.nama);
            $('#jumlah_edit').val(result.jumlah_barang);
            $('#harga_edit').val(result.harga_barang);
            $('#modal-edit').modal('show'); //now its working
        },
        error: function(error) {
            alert("fail");
        }
    });
}

function hapus(id_barang) {
    var url = '<?= base_url() ?>admin/barang/get_barang_by_id/' + id_barang;
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function(result) {
            $('#id_barang_delete').val(result.id_barang);
            $('#nama_delete').val(result.nama);
            $('#jumlah_delete').val(result.jumlah_barang);
            $('#harga_delete').val(result.harga_barang);
            $('#modal-delete').modal('show'); //now its working
        },
        error: function(result) {
            alert("fail");
        }
    });
}
</script>