<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>admin/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?= $title ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Default box -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <form action="<?= base_url() ?>admin/penjualan/cari" method="POST">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#modal-add" style="width: 60%">
                                            <i class="fa fa-plus"></i> Tambah Data
                                        </button>
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <select class="form-control" name="id_barang">
                                            <option value="">Pilih Barang</option>
                                            <?php foreach ($nama_barang as $key => $nm) { ?>
                                            <option value="<?= $nm['id_barang'] ?>" <?php if($search['id_barang'] == $nm['id_barang']) { echo "selected"; } ?> ><?= $nm['nama'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <select class="form-control" name="bulan">
                                            <option value="">Pilih Bulan</option>
                                            <option value="1" <?php if($search['bulan'] == '1') { echo "selected"; } ?> >January</option>
                                            <option value="2" <?php if($search['bulan'] == '2') { echo "selected"; } ?> >February</option>
                                            <option value="3" <?php if($search['bulan'] == '3') { echo "selected"; } ?> >Maret</option>
                                            <option value="4" <?php if($search['bulan'] == '4') { echo "selected"; } ?> >April</option>
                                            <option value="5" <?php if($search['bulan'] == '5') { echo "selected"; } ?> >Mei</option>
                                            <option value="6" <?php if($search['bulan'] == '6') { echo "selected"; } ?> >Juni</option>
                                            <option value="7" <?php if($search['bulan'] == '7') { echo "selected"; } ?> >Juli</option>
                                            <option value="8" <?php if($search['bulan'] == '8') { echo "selected"; } ?> >Agustus</option>
                                            <option value="9" <?php if($search['bulan'] == '9') { echo "selected"; } ?> >September</option>
                                            <option value="10" <?php if($search['bulan'] == '10') { echo "selected"; } ?> >Oktober</option>
                                            <option value="11" <?php if($search['bulan'] == '11') { echo "selected"; } ?> >Novenber</option>
                                            <option value="12" <?php if($search['bulan'] == '12') { echo "selected"; } ?> >Desember</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <select class="form-control" name="tahun">
                                            <option value="">Pilih Tahun</option>
                                            <?php foreach ($rs_tahun as $key => $th) { ?>
                                            <option value="<?= $th['tahun'] ?>" <?php if($search['tahun'] == $th['tahun'] ) { echo "selected"; } ?> ><?= $th['tahun'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="submit" name="save" value="cari" class="btn btn-block btn-success btn-sm" style="width: 100%">
                                            <i class="fa fa-search"></i> Cari
                                        </button>
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="submit" name="save" value="reset" class="btn btn-block btn-dark btn-sm" style="width: 100%">
                                            <i class="fa fa-times"></i> Reset
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width: 10px">No</th>
                                        <th>Nama Barang</th>
                                        <th>Harga Barang</th>
                                        <th>Jumlah penjualan</th>
                                        <th>Tanggal penjualan</th>
                                        <th>Harga</th>
                                        <!-- <th style="width: 200px">Aksi</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($rs_penjualan as $key => $value) { ?>
                                    <tr>
                                        <td align="center"><?= $no++ ?></td>
                                        <td><?= $value['nama'] ?></td>
                                        <td>Rp <?= number_format($value['harga_barang'], 0, ',', '.') ?></td>
                                        <td><?= $value['jumlah_jual'] ?></td>
                                        <td><?= date('d F Y', strtotime($value['tgl_jual']))  ?></td>
                                        <td>Rp <?= number_format($value['harga_jual'], 0, ',', '.') ?></td>
                                        <!-- <td align="center">
                                            <a class="btn btn-info btn-sm edit_mahasiswa" href="#"
                                                onclick="update(<?= $value['id_penjualan'] ?>)"><i
                                                    class="fas fa-pencil-alt"></i> Edit</a>
                                            <a class="btn btn-danger btn-sm" href="#"
                                                onclick="hapus(<?= $value['id_penjualan'] ?>)"><i class="fas fa-trash"></i>
                                                Delete</a>
                                        </td> -->
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <ul class="pagination pagination-sm m-0 float-right">
                                <?=  $this->pagination->create_links(); ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modal-add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data <?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url() ?>admin/penjualan/insert" method="POST">
                <div class="modal-body">

                    <div class="form-group">
                        <label for="id_barang_ad">Nama Barang</label>
                        <select class="form-control select2" id="id_barang_add" onchange="set_jumlah(this)" name="id_barang" required>
                            <option value="">---</option>
                            <?php foreach ($nama_barang as $key => $nm) { ?>
                            <option value="<?= $nm['id_barang'] ?>" data-jumlah="<?= $nm['jumlah_barang'] ?>" data-harga="<?= $nm['harga_barang'] ?>"><?= $nm['nama'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="row">

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Jumlah Barang</label>
                                <input type="number" readonly class="form-control" id="jumlah_barang_res" placeholder="Jumlah Barang">
                                <small>*ketersediaan barang saat ini</small>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Harga Barang</label>
                                <input type="text" readonly class="form-control" id="harga_barang_res" placeholder="Harga Barang">
                                <small>*harga barang saat ini</small>
                            </div>
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="jumlah">Sisa barang</label>
                                <input type="number" readonly class="form-control" name="sisa_barang" id="jumlah_total" placeholder="Sisa Penjualan">
                                <small>*sisa barang setelah penjualan</small>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="jumlah">Total harga</label>
                                <input type="number" readonly class="form-control" name="harga_jual" id="harga_total" placeholder="Total harga">
                                <small>*total harga</small>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="jumlah">Jumlah penjualan</label>
                                <input type="number" class="form-control" name="jumlah_jual" id="jumlah_jual" onkeyup="hitung()" placeholder="Jumlah penjualan">
                                <small>*jumlah penjualan</small>
                            </div>
                        </div>
                        
                    </div>

                    <div class="form-group">
                        <label for="tgl">Tanggal penjualan</label>
                        <input type="date" class="form-control" name="tgl_jual" id="tgl_add" placeholder="Tanggal penjualan"
                            required>
                    </div>

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Data <?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url() ?>admin/penjualan/update" method="POST">
                <input type="hidden" name="id_penjualan" id="id_penjualan_edit" value="">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="id_barang_edit">Nama Barang</label>
                        <select class="form-control select2" id="id_barang_edit" name="barang" required readonly>
                            <option value="">---</option>
                            <?php foreach ($nama_barang as $nb) : ?>
                            <?php $selected = $penjualan['id_barang'] == $nb['id_barang'] ? 'selected' : ''; ?>
                            <option <?= $selected ?> value="<?= $nb['id_barang'] ?>">
                                <?= $nb['nama'] ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="jumlah">Jumlah penjualan</label>
                        <input type="number" class="form-control" name="jumlah" id="jumlah_edit"
                            placeholder="Jumlah penjualan" required>
                    </div>
                    <div class="form-group">
                        <label for="tgl">Tanggal penjualan</label>
                        <input type="date" class="form-control" name="tgl" id="tgl_edit" placeholder="Tanggal penjualan"
                            required>
                    </div>

                </div>

                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Data <?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url() ?>admin/penjualan/delete" method="POST">
                <input type="hidden" name="id_penjualan" id="id_penjualan_delete" value="">
                <div class="modal-body">

                    <div class="form-group">
                        <label for="id_barang_edit">Nama Barang</label>
                        <select class="form-control select2" id="id_barang_edit" name="barang" required readonly>
                            <option value="">---</option>
                            <?php foreach ($nama_barang as $nb) : ?>
                            <?php $selected = $penjualan['id_barang'] == $nb['id_barang'] ? 'selected' : ''; ?>
                            <option <?= $selected ?> value="<?= $nb['id_barang'] ?>">
                                <?= $nb['nama'] ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="jumlah">Jumlah penjualan</label>
                        <input type="number" class="form-control" name="jumlah" id="jumlah_delete"
                            placeholder="Jumlah penjualan" readonly>
                    </div>
                    <div class="form-group">
                        <label for="tgl">Tanggal penjualan</label>
                        <input type="date" class="form-control" name="tgl" id="tgl_delete" placeholder="Tanggal penjualan"
                            readonly>
                    </div>

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>

function update(id_penjualan) {
    var url = '<?= base_url() ?>admin/penjualan/get_penjualan_by_id/' + id_penjualan;
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function(result) {
            $('#id_penjualan_edit').val(result.id_penjualan);
            $('#id_barang_edit').val(result.nama);
            $('#jumlah_edit').val(result.jumlah_penjualan);
            $('#tgl_edit').val(result.tgl_penjualan);
            $('#modal-edit').modal('show'); //now its working
        },
        error: function(error) {
            alert("fail");
        }
    });
}

function hapus(id_penjualan) {
    var url = '<?= base_url() ?>admin/penjualan/get_penjualan_by_id/' + id_penjualan;
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function(result) {
            $('#id_penjualan_delete').val(result.id_penjualan);
            $('#nama_delete').val(result.nama);
            $('#jumlah_delete').val(result.jumlah_penjualan);
            $('#tgl_delete').val(result.tgl_penjualan);
            $('#modal-delete').modal('show'); //now its working
        },
        error: function(result) {
            alert("fail");
        }
    });
}
</script>

<script type="text/javascript">
    function set_jumlah(selectObject){
        var jumlah_barang = selectObject.options[selectObject.selectedIndex].getAttribute('data-jumlah');
        var harga_barang = selectObject.options[selectObject.selectedIndex].getAttribute('data-harga');
        document.getElementById("jumlah_barang_res").value = jumlah_barang;
        document.getElementById("harga_barang_res").value = harga_barang;
    }

    function hitung(){
        const awal = document.getElementById("jumlah_barang_res").value;
        const harga = document.getElementById("harga_barang_res").value;
        const minus = document.getElementById("jumlah_jual").value;
        // 
        const hasil = Number(awal) - Number(minus);
        const harga_tot = Number(harga) * Number(minus);
        document.getElementById("jumlah_total").value = hasil;
        document.getElementById("harga_total").value = harga_tot;
        // 
        if (Number(document.getElementById("jumlah_jual").value) > Number(document.getElementById("jumlah_barang_res").value)) {
            alert('jumlah penjualan melebihi stok barang yang ada');
            document.getElementById("jumlah_jual").value = null;
            document.getElementById("jumlah_total").value = null;
            document.getElementById("harga_total").value = null;
        }
        
    }
</script>