<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Dashboard</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-12">
                <div class="info-box">
                    <span class="info-box-icon bg-info"><i class="fa fa-upload"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Penjualan</span>
                        <span class="info-box-number"><?= $summary['jum_penjualan'] ?></span>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-12">
                <div class="info-box">
                    <span class="info-box-icon bg-warning"><i class="fa fa-download"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Stok</span>
                        <span class="info-box-number"><?= $summary['jum_stok'] ?></span>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-12">
                <div class="info-box">
                    <span class="info-box-icon bg-success"><i class="fa fa-database"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Barang</span>
                        <span class="info-box-number"><?= $summary['jum_barang'] ?></span>
                    </div>
                </div>
            </div>

        </div>
          
        <!-- Default box -->
        
        <!-- /.card -->

    </section>
</div>