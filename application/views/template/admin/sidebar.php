<!-- Sidebar Menu -->
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="<?= base_url() ?>admin/dashboard"
                class="nav-link <?php if( $title == 'Dashboard') {echo "active";}?>">
                <i class="nav-icon icon fas fa-tachometer-alt"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        <li class="nav-header">DATA MASTER</li>

        <li class="nav-item">
            <a href="<?= base_url() ?>admin/penjualan" class="nav-link <?php if( $title == 'Penjualan') {echo "active";}?>">
                <i class="nav-icon fa fa-upload"></i>
                <p>
                    Data Penjualan
                </p>
            </a>
        </li>

        <li class="nav-item">
            <a href="<?= base_url() ?>admin/stok" class="nav-link <?php if( $title == 'Stok') {echo "active";}?>">
                <i class="nav-icon fa fa-download"></i>
                <p>
                    Data Stok
                </p>
            </a>
        </li>

        <li class="nav-item">
            <a href="<?= base_url() ?>admin/barang" class="nav-link <?php if( $title == 'Barang') {echo "active";}?>">
                <i class="nav-icon fa fa-database"></i>
                <p>
                    Data Barang
                </p>
            </a>
        </li>

        
    </ul>
</nav>
<!-- /.sidebar-menu -->